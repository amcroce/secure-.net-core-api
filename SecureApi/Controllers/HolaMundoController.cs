﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace SecureApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HolaMundoController : Controller
    {
        [HttpGet]
        [Authorize(Roles = "Técnico")]
        public async Task<IActionResult> Hola()
        {
            return Ok("Hola Mundo");
        }
    }
}